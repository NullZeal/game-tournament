# Game Tournament Announcer

An announcer for game tournaments!

## Features

- Picks a random game from a pool.
- Handles basic team matchmaking.
- Announces the results with text-to-speech.
- Displays the results in ASCII art.
- Plays background music.

## Limitations

- Only tested on Windows 10.

## Before You Start

1. Edit [game_pool.conf](config/game_pool.conf) and put in the games of your tournament.
1. Edit [team_pool.conf](config/team_pool.conf) and put in the teams of your tournament.
1. Lower your volume to 10%. This thing is pretty loud.

## How to Start

Double-click on `play.bat`

## License

Please see the [LICENSE](LICENSE) file.

## Attribution

### Music Attribution

- https://freesound.org/people/frankum/
- https://freesound.org/people/joshuaempyre/
- https://freesound.org/people/RutgerMuller/

### Sound Effects Attribution
- Sound Effects provided by Artist Julien Pontbriand

### Tool/Library Attribution

- ASCII art generation is made possible by the [FIGlet](http://www.figlet.org/) program and this online [FIGlet API](https://helloacm.com/figlet/).

### Logo Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
