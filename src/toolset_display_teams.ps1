function Show-ChosenTeams()
{
    $teamPool = Read-ConfigPool "config/team_pool.conf"
    $team1 = $teamPool | Get-Random
    $teamPool.Remove($team1)
    $team2 = $teamPool | Get-Random

    Show-AsciiArt -Text "The chosen teams are..." -Color "Red"
    Start-Sleep -Milliseconds 1500
    Show-OneWordPerLine "$team1 versus $team2" -Color "Cyan"
}
