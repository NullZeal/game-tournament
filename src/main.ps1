$ErrorActionPreference = "Stop"
$ProgressPreference = "SilentlyContinue"

. $PSScriptRoot/toolset_audio_music.ps1
. $PSScriptRoot/toolset_audio_sound_effects.ps1
. $PSScriptRoot/toolset_audio_speech.ps1
. $PSScriptRoot/toolset_display_ascii.ps1
. $PSScriptRoot/toolset_display_games.ps1
. $PSScriptRoot/toolset_display_teams.ps1
. $PSScriptRoot/toolset_io_config_file.ps1
. $PSScriptRoot/toolset_io_random_file.ps1

Clear-Host

Play-Intro-Sound-Effects
Start-RandomMusic

Read-Host "(press ENTER to reveal teams)"
Show-ChosenTeams

Read-Host "(press ENTER to reveal game)"
Show-ChosenGame

Read-Host "(press ENTER to start countdown)"
Clear-Host
Play-Finale-Sound-Effects

Read-Host "(press ENTER to exit)"
Play-Toasty

Exit 0