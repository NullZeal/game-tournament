Add-Type -AssemblyName System.Speech
$Speech = New-Object System.Speech.Synthesis.SpeechSynthesizer
$Speech.SelectVoice('Microsoft Zira Desktop')
$Speech.SetOutputToDefaultAudioDevice()
