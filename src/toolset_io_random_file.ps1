function Get-RandomFile(
    [string] $DirectoryPath,
    [string] $Filter = "*.*")
{
    if(Test-Path $DirectoryPath)
    {
        $searchPath = Join-Path -Path $DirectoryPath -ChildPath "*"
        $fileList = Get-ChildItem -Path $searchPath -Include $Filter -File
        
        if($fileList.Count -eq 0)
        {
            return $null
        }
        elseif($fileList.Count -eq 1)
        {
            return $fileList[0]
        }
        else
        {
            return Get-Random $fileList
        }
    }
    else
    {
        return $null
    }
}
