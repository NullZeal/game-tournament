function Show-ChosenGame()
{
    $gamePool = Read-ConfigPool "config/game_pool.conf"
    $gameTitle = $gamePool | Get-Random

    Show-AsciiArt -Text "The chosen game is..." -Color "Red"
    Start-Sleep -Milliseconds 1500
    Show-OneWordPerLine $gameTitle -Color "Green"
    Show-AsciiArt -Text "LET'S GO!!" -Color "Red"
}
