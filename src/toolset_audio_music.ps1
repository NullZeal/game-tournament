function Start-RandomMusic()
{
    $musicFile = Get-RandomFile "music" -Filter "*.wav"

    if($musicFile)
    {
        $player = New-Object System.Media.SoundPlayer
        $player.SoundLocation = $musicFile
        $player.PlayLooping()
    }
}
