function Play-Laugh()
{
    $soundEffectFile = Get-RandomFile "sound_effects" -Filter "*laugh.wav"

    if($soundEffectFile)
    {
        $player = New-Object System.Media.SoundPlayer
        $player.SoundLocation = $soundEffectFile
        $player.Play()
        Start-Sleep -Milliseconds 2800
    }
}

function Play-Birthday()
{
    $soundEffectFile = Get-RandomFile "sound_effects" -Filter "*birthday.wav"

    if($soundEffectFile)
    {
        $player = New-Object System.Media.SoundPlayer
        $player.SoundLocation = $soundEffectFile
        $player.Play()
        Start-Sleep -Milliseconds 3500
    }
}

function Play-Intro-Sound-Effects()
{
    Play-Laugh
    Play-Birthday
}

function Play-5-To-1()
{
    $counter = 5

    for ($i = 5; $i -gt 0; $i--) {

        $soundEffectFile = Get-RandomFile "sound_effects" -Filter "*${counter}.wav"

        if($soundEffectFile)
        {
            $player = New-Object System.Media.SoundPlayer
            $player.SoundLocation = $soundEffectFile
            $player.Play()
            Show-AsciiArt -Text "${counter}" -Color "Red" -IncludeTextToSpeech 0
            Start-Sleep -Milliseconds 1000
        }
        $counter -= 1
        Clear-Host
    }
}

function Play-What-Are-You-Waiting-For()
{
    $soundEffectFile = Get-RandomFile "sound_effects" -Filter "*_for.wav"

    if($soundEffectFile)
    {
        $player = New-Object System.Media.SoundPlayer
        $player.SoundLocation = $soundEffectFile
        Start-Sleep -Milliseconds 1500
        $player.Play()
        Start-Sleep -Milliseconds 500
        Show-AsciiArt -Text "WHAT ARE YOU WAITING FOR?" -Color "Red" -IncludeTextToSpeech 0
        Start-Sleep -Milliseconds 3000
    }
}

function Play-Toasty()
{
    $soundEffectFile = Get-RandomFile "sound_effects" -Filter "*toasty.wav"

    if($soundEffectFile)
    {
        $player = New-Object System.Media.SoundPlayer
        $player.SoundLocation = $soundEffectFile
        $player.Play()
        Start-Sleep -Milliseconds 2500
    }
}

function Play-Finale-Sound-Effects(){

    Play-5-To-1
    Play-What-Are-You-Waiting-For
}