function Show-AsciiArt(
    [string] $Text,
    [string] $Color)
{
    $uri = "https://uploadbeta.com/api/figlet/?cached&msg=$Text"
    $response = Invoke-WebRequest -Uri $uri -UseBasicParsing
    $response = $response | ConvertFrom-Json
    Write-Host $response -ForegroundColor $Color
    $Speech.Speak($Text)
}

function Show-OneWordPerLine(
    [string] $Sentence,
    [string] $Color)
{
    foreach($word in $Sentence -split " ")
    {
        Show-AsciiArt -Text $word -Color $Color
    }
}

function Show-AsciiArt-No-Text-To-Speech(
    [string] $Text,
    [string] $Color)
{
    $uri = "https://uploadbeta.com/api/figlet/?cached&msg=$Text"
    $response = Invoke-WebRequest -Uri $uri -UseBasicParsing
    $response = $response | ConvertFrom-Json
    Write-Host $response -ForegroundColor $Color
}

function Show-OneWordPerLine(
    [string] $Sentence,
    [string] $Color)
{
    foreach($word in $Sentence -split " ")
    {
        Show-AsciiArt -Text $word -Color $Color
    }
}
